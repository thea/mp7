### What is this repository for? ###

* This repository mirrors and extends the official `mp7` `cactus` firmware repository
* The `master` and `cactus` branches are exact copies of the svn `trunk`
* The `master-standalone` is based on master, 

### How do I get set up? ###

The master firmware uses the [ipbb](https://github.com/ipbus/ipbb) build tool, and requires the [ipbus system firmware](https://github.com/ipbus/ipbus-firmware).
The following example procedure should build an example board image.
operating system (e.g. Centos7) is required.  You will need to run the scripts using python2.7 (not python3).  If you are going to build on a computer outside of the CERN network, then you will need to run kerberos (`kinit username@CERN.CH`).
These instructions assume that you have your Xilinx Vivado licensing already setup for your enviroment.

    # Download and install ipbb
    curl -L https://github.com/ipbus/ipbb/archive/v0.2.8.tar.gz | tar xvz
    source ipbb-0.2.8/env.sh
    
    # Create a local working area
    ipbb init build
    cd build
    ipbb add git https://github.com/ipbus/ipbus-firmware.git -b master
    ipbb add git https://:@gitlab.cern.ch:8443/cms-cactus/firmware/mp7.git -b master
    
    # Create the example project 
    ipbb proj create vivado mp7xe_690 mp7:projects/examples/mp7xe_690
    cd proj/mp7xe_690
    ipbb vivado project
    
    # Run implementation, syntesys
    ipbb vivado synth
    ipbb vivado impl
    
    # Generate a bitfile
    ipbb vivado package
    deactivate

### Who do I talk to? ###

* Alessandro Thea (alessandro.thea@cern.ch)
* Dave Newbold (dave.newbold@cern.ch)
* Greg Iles (gregory.iles@cern.ch)
