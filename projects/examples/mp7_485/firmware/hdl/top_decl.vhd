-- top_decl
--
-- Defines constants for the whole device
--
-- Dave Newbold, June 2014

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.mp7_top_decl.all;

package top_decl is
  
	constant ALGO_REV: std_logic_vector(31 downto 0) := X"00010000";
	constant BUILDSYS_BUILD_TIME: std_logic_vector(31 downto 0) := X"00000000"; -- To be overwritten at build time
	constant BUILDSYS_BLAME_HASH: std_logic_vector(31 downto 0) := X"00000000"; -- To be overwritten at build time
	
	constant LHC_BUNCH_COUNT: integer := 3564;
	constant LB_ADDR_WIDTH: integer := 10;
	constant DR_ADDR_WIDTH: integer := 9;
	constant RO_CHUNKS: integer := 32;
	constant CLOCK_RATIO: integer := 6;
	constant CLOCK_AUX_RATIO: clock_ratio_array_t := (2, 4, 4);
	constant PAYLOAD_LATENCY: integer := 2;
	constant DAQ_N_BANKS: integer := 4; -- Number of readout banks
	constant DAQ_TRIGGER_MODES: integer := 2; -- Number of trigger modes for readout
	constant DAQ_N_CAP_CTRLS: integer := 4; -- Number of capture controls per trigger mode
	constant ZS_ENABLED: boolean := FALSE;
		
	constant REGION_CONF: region_conf_array_t := (
		(gth_10g, u_crc32, 1, 8, 3), -- 0
		(gth_10g, u_crc32, 1, 7, 3), -- 1
		(gth_10g, u_crc32, 1, 6, 4), -- 2
		(gth_10g, u_crc32, 1, 5, 4), -- 3
		(gth_10g, u_crc32, 1, 4, 5), -- 4
		(gth_10g, u_crc32, 1, 3, 5), -- 5
		(gth_10g, u_crc32, 1, 2, 7), -- 6
		(gth_10g, u_crc32, 1, 1, 7), -- 7
		(gth_10g, u_crc32, 1, 0, 7), -- 8
		(gth_10g, u_crc32, 0, 0, 6), -- 9
		(gth_10g, u_crc32, 0, 1, 6), -- 10
		(gth_10g, u_crc32, 0, 2, 2), -- 11
		(gth_10g, u_crc32, 0, 3, 2), -- 12
		(gth_10g, u_crc32, 0, 4, 1), -- 13
		(gth_10g, u_crc32, 0, 5, 1), -- 14
		(gth_10g, u_crc32, 0, 6, 0), -- 15
		(gth_10g, u_crc32, 0, 7, 0), -- 16
		(gth_10g, u_crc32, 0, 8, 0) -- 17
	);

end top_decl;

